################################
Godot offscreen target indicator
################################

This project is (as its name suggests) intended to work with the `Godot
<https://godotengine.org/>`_.

The Godot offscreen target indicator provide a simple scene that spawn arrow
(by default) pointing to an offscreen target while always sticking to the edge of
the screen.

Usage
=====

There is two separate scenes that you can use in this addon.

POI
---

This scene is a simple Node2D that automatically places itself at the center of the
screen when ready. It also has methods to add or remove target indicators as childrens.

The POI scene is intended to be a child of a CanvasLayer node (such as your
HUD). Then your main scene can use the methods add_indicator() or remove_indicator()
to manage them during the course of the game.  The game also emit the two signals
"indicator_added" and "indicator_removed" when needed.

The POI scene also propagate the "size_changed" signal of the main viewport to
its childrens (which it assumes are class or subclass of TargetIndicator). If this
isn't wanted just untick the "Propagate screen changes" boolean property in the editor.

TargetIndicator
---------------

This scene can be used with or without the POI scene. It expect to be the child of
a scene (inheriting from Node2D, NOT Control) positionned at the center of the screen. 

This class can be subclassed, thus allowing to create different indicators by changing
the sprites or even adding some infos to it. Moving the sprite of the scene on the
y axis allow to augment or reduce the margin between the indicator and the screen edge.

Authors and licence
===================

Godot offscreen target indicator is initially written by Guilhem MAS-PAITRAULT
for Koyasik.

The following program is licensed under the MIT license.

The arrow asset (addons/offscreen_target_indicator/indicator_sprite.svg) can also
be used under the CC-0.
