# POI class: Points Of Interest Node2D handling TargetIndicators
#
# This class is intended to be put into a godot CanvasLayer and is intended to hold
# and manage target indicators. It provides functions to add and remove 
extends Node2D

signal indicator_added
signal indicator_removed

export(bool) var propagate_screen_change = true


func _ready():
    position = OS.window_size / 2
    if propagate_screen_change:
        get_node("/root").get_viewport().connect("size_changed", self, "screen_size_changed")


func add_indicator(indicator, target, origin):
    var new_indicator = indicator.instance()
    new_indicator.name = target.name
    new_indicator.target = target
    new_indicator.origin = origin
    add_child(new_indicator)
    emit_signal("indicator_added")


func remove_indicator(target):
    var node = get_node(target.name)
    if node:
        node.queue_free()
        emit_signal("indicator_removed")


func screen_size_changed():
    for c in get_children():
        c.set_screen_size()
